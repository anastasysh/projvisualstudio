﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using genPsswdCore;

namespace LabGenPasswd
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            buttonGen.Click += delegate
            {
                textBoxPsswd.Text = Utils.RandomStr((int)numericUpDownLenth.Value, checkBoxDown.Checked, checkBoxUp.Checked, checkBoxNumer.Checked, checkBoxSymbol.Checked);

            };
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
