﻿namespace LabGenPasswd
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxPsswd = new System.Windows.Forms.TextBox();
            this.buttonGen = new System.Windows.Forms.Button();
            this.checkBoxDown = new System.Windows.Forms.CheckBox();
            this.checkBoxUp = new System.Windows.Forms.CheckBox();
            this.checkBoxNumer = new System.Windows.Forms.CheckBox();
            this.checkBoxSymbol = new System.Windows.Forms.CheckBox();
            this.numericUpDownLenth = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLenth)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxPsswd
            // 
            this.textBoxPsswd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPsswd.Location = new System.Drawing.Point(129, 169);
            this.textBoxPsswd.Name = "textBoxPsswd";
            this.textBoxPsswd.ReadOnly = true;
            this.textBoxPsswd.Size = new System.Drawing.Size(438, 41);
            this.textBoxPsswd.TabIndex = 0;
            this.textBoxPsswd.Text = "твой новый убогий пароль";
            this.textBoxPsswd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGen
            // 
            this.buttonGen.Location = new System.Drawing.Point(257, 222);
            this.buttonGen.Name = "buttonGen";
            this.buttonGen.Size = new System.Drawing.Size(161, 85);
            this.buttonGen.TabIndex = 1;
            this.buttonGen.Text = "Генерировать";
            this.buttonGen.UseVisualStyleBackColor = true;
            // 
            // checkBoxDown
            // 
            this.checkBoxDown.AutoSize = true;
            this.checkBoxDown.Location = new System.Drawing.Point(257, 324);
            this.checkBoxDown.Name = "checkBoxDown";
            this.checkBoxDown.Size = new System.Drawing.Size(253, 24);
            this.checkBoxDown.TabIndex = 2;
            this.checkBoxDown.Text = "Символы в нижнем регистре";
            this.checkBoxDown.UseVisualStyleBackColor = true;
            // 
            // checkBoxUp
            // 
            this.checkBoxUp.AutoSize = true;
            this.checkBoxUp.Location = new System.Drawing.Point(257, 354);
            this.checkBoxUp.Name = "checkBoxUp";
            this.checkBoxUp.Size = new System.Drawing.Size(258, 24);
            this.checkBoxUp.TabIndex = 3;
            this.checkBoxUp.Text = "Символы в верхнем регистре";
            this.checkBoxUp.UseVisualStyleBackColor = true;
            // 
            // checkBoxNumer
            // 
            this.checkBoxNumer.AutoSize = true;
            this.checkBoxNumer.Location = new System.Drawing.Point(257, 384);
            this.checkBoxNumer.Name = "checkBoxNumer";
            this.checkBoxNumer.Size = new System.Drawing.Size(91, 24);
            this.checkBoxNumer.TabIndex = 4;
            this.checkBoxNumer.Text = "Цифры";
            this.checkBoxNumer.UseVisualStyleBackColor = true;
            // 
            // checkBoxSymbol
            // 
            this.checkBoxSymbol.AutoSize = true;
            this.checkBoxSymbol.Location = new System.Drawing.Point(257, 414);
            this.checkBoxSymbol.Name = "checkBoxSymbol";
            this.checkBoxSymbol.Size = new System.Drawing.Size(140, 24);
            this.checkBoxSymbol.TabIndex = 5;
            this.checkBoxSymbol.Text = "Спецсимволы";
            this.checkBoxSymbol.UseVisualStyleBackColor = true;
            // 
            // numericUpDownLenth
            // 
            this.numericUpDownLenth.Location = new System.Drawing.Point(337, 113);
            this.numericUpDownLenth.Name = "numericUpDownLenth";
            this.numericUpDownLenth.Size = new System.Drawing.Size(120, 26);
            this.numericUpDownLenth.TabIndex = 6;
            this.numericUpDownLenth.Tag = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(183, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Длинна пароля";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownLenth);
            this.Controls.Add(this.checkBoxSymbol);
            this.Controls.Add(this.checkBoxNumer);
            this.Controls.Add(this.checkBoxUp);
            this.Controls.Add(this.checkBoxDown);
            this.Controls.Add(this.buttonGen);
            this.Controls.Add(this.textBoxPsswd);
            this.Name = "Form1";
            this.Text = "77";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLenth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxPsswd;
        private System.Windows.Forms.Button buttonGen;
        private System.Windows.Forms.CheckBox checkBoxDown;
        private System.Windows.Forms.CheckBox checkBoxUp;
        private System.Windows.Forms.CheckBox checkBoxNumer;
        private System.Windows.Forms.CheckBox checkBoxSymbol;
        private System.Windows.Forms.NumericUpDown numericUpDownLenth;
        private System.Windows.Forms.Label label1;
    }
}

