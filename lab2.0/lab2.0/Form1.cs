﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2._0
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.KeyDown += Keydown;
        }

        private void Keydown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode) {

                case Keys.Left:
                    laText.Text = "Left";
                    break;
                case Keys.Right:
                    laText.Text = "Right";
                    break;
                case Keys.Up:
                    laText.Text = "Up";
                    break;
                case Keys.Down:
                    laText.Text = "Down";
                    break;
                case Keys.Space:
                    if (e.Shift)
                    {
                        laText.Text = "Shift +space";
                    }
                    else {
                        laText.Text = "Space";
                    }
                    break;
            }
       
        }
    }
}
