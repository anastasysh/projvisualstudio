﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// String.Builder String.format 
/// необработанные исключения. места где может возникнуть работа не по плану.
/// @ - для экранирования всей строчки.
/// try catch (Ecseption ex) надо ставить в конце
/// finaly выполняется автоматически вне зависимости от результата try
/// 
/// </summary>

namespace lab2._0
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
