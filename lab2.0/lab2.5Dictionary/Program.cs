﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab2._5Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>(5);
            x.Add(1, "A");
            x.Add(2, "B");
            x.Add(3, "C");
            x.Add(4, "D");
            x.Add(5, "E");

            foreach (KeyValuePair<int, string> keyValue in x)
            {
                Console.WriteLine(keyValue.Key + "-" + keyValue.Value);
            }
        }
    }
}
