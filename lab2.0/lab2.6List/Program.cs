﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab2._6List
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> x = new List<int>(){1, 2, 3, 4, 5};
            x.Add(6);
            x.AddRange(new int[] { 7, 8, 9 });
            x.Insert(0, 500);
            x.RemoveAt(1);
            foreach (var i in x)
            {
                Console.WriteLine(i);
            }
        }
    }
}
