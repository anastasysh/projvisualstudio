﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class fileExplorer : Form
    {
        private string _curDir;

        public fileExplorer()
        {
            InitializeComponent();

            CurDir = Directory.GetCurrentDirectory();
            buUp.Click += (s, e) => LoadDir(Directory.GetParent(CurDir).ToString());
            lv.ItemSelectionChanged += (s, e) => SelectedItem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += (s, e) => LoadDir(SelectedItem);
            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;
            //var c1 = new ColumnHeader();
            //c1.Text = "Имя";
            //c1.Width = 350;
            //lv.Columns.Add(c1);
            lv.Columns.Add(new ColumnHeader { Text = "Имя", Width = 350 });
            lv.Columns.Add("Дата изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 150);
            Text += " Drivers=" + string.Join(" ", Directory.GetLogicalDrives());
            LoadDir(CurDir);
        }

        private void LoadDir(string newDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                //lv.Items.Add(item.Name, 0);
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", "" },
                    0
                    ));
            }
            foreach (var item in directoryInfo.GetFiles())
            {
                //lv.Items.Add(item.Name, 1);
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", item.Length.ToString() },
                    1
                    ));
            }
            lv.EndUpdate();
            CurDir = newDir;
        }

        public string CurDir
        {
            get { return _curDir; }
            private set { _curDir = value; edDir.Text = _curDir; }
        }
        public string SelectedItem { get; private set; }


    }
}
