﻿namespace labTrainerAccount
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.laCorrect = new System.Windows.Forms.Label();
            this.laIncorrect = new System.Windows.Forms.Label();
            this.bCorrect = new System.Windows.Forms.Button();
            this.bIncorrect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.laCode = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.laCorrect, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.laIncorrect, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(581, 138);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.bIncorrect, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.bCorrect, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 442);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(581, 147);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // laCorrect
            // 
            this.laCorrect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.laCorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laCorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laCorrect.Location = new System.Drawing.Point(3, 0);
            this.laCorrect.Name = "laCorrect";
            this.laCorrect.Size = new System.Drawing.Size(284, 138);
            this.laCorrect.TabIndex = 0;
            this.laCorrect.Text = "Correct = 0";
            this.laCorrect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laIncorrect
            // 
            this.laIncorrect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.laIncorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laIncorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.laIncorrect.Location = new System.Drawing.Point(293, 0);
            this.laIncorrect.Name = "laIncorrect";
            this.laIncorrect.Size = new System.Drawing.Size(285, 138);
            this.laIncorrect.TabIndex = 1;
            this.laIncorrect.Text = "Incorrect = 0";
            this.laIncorrect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bCorrect
            // 
            this.bCorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bCorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.bCorrect.ForeColor = System.Drawing.Color.Green;
            this.bCorrect.Location = new System.Drawing.Point(3, 3);
            this.bCorrect.Name = "bCorrect";
            this.bCorrect.Size = new System.Drawing.Size(284, 141);
            this.bCorrect.TabIndex = 0;
            this.bCorrect.Text = "Yes";
            this.bCorrect.UseVisualStyleBackColor = true;
            // 
            // bIncorrect
            // 
            this.bIncorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bIncorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.bIncorrect.ForeColor = System.Drawing.Color.Maroon;
            this.bIncorrect.Location = new System.Drawing.Point(293, 3);
            this.bIncorrect.Name = "bIncorrect";
            this.bIncorrect.Size = new System.Drawing.Size(285, 141);
            this.bIncorrect.TabIndex = 1;
            this.bIncorrect.Text = "No";
            this.bIncorrect.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label3.Location = new System.Drawing.Point(14, 401);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(581, 38);
            this.label3.TabIndex = 2;
            this.label3.Text = "Correct?";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laCode
            // 
            this.laCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.laCode.Location = new System.Drawing.Point(12, 153);
            this.laCode.Name = "laCode";
            this.laCode.Size = new System.Drawing.Size(570, 248);
            this.laCode.TabIndex = 3;
            this.laCode.Text = "10 + 11 = 21";
            this.laCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 615);
            this.Controls.Add(this.laCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "fm";
            this.Text = "labTrainerAccount";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label laCorrect;
        private System.Windows.Forms.Label laIncorrect;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button bIncorrect;
        private System.Windows.Forms.Button bCorrect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label laCode;
    }
}

