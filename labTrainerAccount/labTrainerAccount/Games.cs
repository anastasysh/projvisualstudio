﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labTrainerAccount
{
    class Games
    {
        public int CountCorrect { get; protected set; }
        public int CountIncorrect { get; protected set; }
        public bool AnswerCorrect { get; protected set; }
        public string CodeText { get; protected set; }

        public event EventHandler Change;

        public void Reset()
        {
            CountCorrect = 0;
            CountIncorrect = 0;
            doContinue();
        }

        public void doContinue()
        {
            Random rnd = new Random();
            int value1 = rnd.Next(20);
            int value2 = rnd.Next(20);
            int result = value1 + value2;
            int resultNew;
            int sign;
            if (rnd.Next(2) == 1)
                resultNew = result;
            else
            {
                if (rnd.Next(2) == 1)
                    sign = 1;
                else
                    sign = -1;
                resultNew = result + (rnd.Next(7) * sign);
            }
            AnswerCorrect = (result == resultNew);
            CodeText = String.Format("{0} + {1} = {2}", value1, value2, resultNew);
            if (Change != null)
                Change(this, EventArgs.Empty);
        }

        public void doAnswer(bool ans)
        {
            if (ans == AnswerCorrect)
                CountCorrect++;
            else
                CountIncorrect++;
            doContinue();
        }
    }
}
