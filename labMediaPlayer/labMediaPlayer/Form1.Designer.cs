﻿namespace labMediaPlayer
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buPause = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.trVolume = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.trVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // buPlay
            // 
            this.buPlay.Location = new System.Drawing.Point(14, 85);
            this.buPlay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(84, 29);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "play";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // buPause
            // 
            this.buPause.Location = new System.Drawing.Point(130, 85);
            this.buPause.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buPause.Name = "buPause";
            this.buPause.Size = new System.Drawing.Size(84, 29);
            this.buPause.TabIndex = 1;
            this.buPause.Text = "pause";
            this.buPause.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Location = new System.Drawing.Point(249, 85);
            this.buStop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(84, 29);
            this.buStop.TabIndex = 2;
            this.buStop.Text = "stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Volume";
            // 
            // trVolume
            // 
            this.trVolume.Location = new System.Drawing.Point(29, 254);
            this.trVolume.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trVolume.Name = "trVolume";
            this.trVolume.Size = new System.Drawing.Size(324, 69);
            this.trVolume.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 562);
            this.Controls.Add(this.trVolume);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPause);
            this.Controls.Add(this.buPlay);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "labMediaPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.trVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buPause;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trVolume;
    }
}

