﻿using labImageOnTable.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageOnTable
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            List<PictureBox> list = new List<PictureBox>
            {
                pictureBox1,
                pictureBox2,
                pictureBox3,
                pictureBox4,
                pictureBox5,
                pictureBox6,
                pictureBox7,
                pictureBox8,
                pictureBox9
            };
            //
            int i = 0;
            foreach (var x in list)
            {
                x.MouseEnter += X_MouseEnter;
                x.MouseLeave += X_MouseLeave;
                x.Image = imageList1.Images[0];
                x.Tag = ++i;
                x.BackgroundImage = Resources.backgr;
            }
        }

        private void X_MouseLeave(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
                x.Image = imageList1.Images[0];
        }

        private void X_MouseEnter(object sender, EventArgs e)
        {
            if (sender is PictureBox x)
                x.Image = imageList1.Images[Convert.ToInt32(x.Tag)];
        }
    }
}
