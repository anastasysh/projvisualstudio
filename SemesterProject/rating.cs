﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterProject
{
    public class rating
    {
        public string name { get; set; }
        public int score{ get; set; }
        public rating (string name, int score){
            this.name = name;
            this.score = score;
    }

    }
}
