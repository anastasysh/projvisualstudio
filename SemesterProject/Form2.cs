﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
    public partial class Form2 : Form
    {
        public int score;
        public string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "scoreTable.txt");
        public Form2(int thisScore)
        {
            InitializeComponent();
            score = thisScore;
            Result.Text = score.ToString(); 
            button1.Click += new EventHandler(addResult);
            button1.DialogResult = DialogResult.OK;
            var xReadText = File.ReadAllText(path);
            string[] separator = { "\r\n"};
            string[] separator2 = { "; "};
            string []res = xReadText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            List <rating> all = new List<rating>();
            for (int i = 0; i < res.Length; i++)
            {
                string[] newres = res[i].Split(separator2, StringSplitOptions.RemoveEmptyEntries);
                rating rat = new rating(newres[0], int.Parse(newres[1]));
                all.Add(rat);
            }
            rating[] newAll = Bubble_Sort(all);
            for (int i = 0; i != 3; i++) {
                getName(i+1).Text = newAll[i].name;
                getScore(i+1).Text = newAll[i].score.ToString(); 
            }
        }

        private void addResult(object sender, EventArgs e)
        {
            Name = yourName.Text;
            if (Name == "") {
                Name = "None";
            }
            string str = Name + "; " + score + "\r\n";
            
            File.AppendAllText(path, str);
           
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            

        }
        public static rating[] Bubble_Sort(List <rating> anArray1)
        {
            rating[] anArray = anArray1.ToArray();
            
         for (int i = 0; i < anArray.Length; i++)
            {
            
                for (int j = 0; j < anArray.Length - 1 - i; j++)
                {
                    if (anArray[j].score < anArray[j + 1].score)
                    {
                        Swap(ref anArray[j], ref anArray[j + 1]);
                    }
                }
            }
            return (anArray);
        }
        public static void Swap(ref rating aFirstArg, ref rating aSecondArg)
        {
            rating tmpParam = aFirstArg;
            aFirstArg = aSecondArg;
            aSecondArg = tmpParam;
        }

        public Label getName(int num)
        {
            switch (num)
            {
                case 1: return name1;
                case 2: return name2;
                case 3: return name3;
                default: return null;
            }
        }

        public Label getScore(int num)
        {
            switch (num)
            {
                case 1: return score1;
                case 2: return score2;
                case 3: return score3;
                default: return null;
            }
        }

        private void Result_Click(object sender, EventArgs e)
        {

        }

        private void name2_Click(object sender, EventArgs e)
        {

        }

        private void name3_Click(object sender, EventArgs e)
        {

        }

        private void score3_Click(object sender, EventArgs e)
        {

        }

        private void score2_Click(object sender, EventArgs e)
        {

        }

        private void name1_Click(object sender, EventArgs e)
        {

        }

        private void score1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void newName_Click(object sender, EventArgs e)
        {

        }
    }
}
