﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSelectInGrid
{
    public partial class fm : Form
    {
        private Bitmap b;

        public int CellWidth { get; private set; }
        public int CellHeigt { get; private set; }
        public int Cols { get; private set; } = 4;
        public int Rows { get; private set; } = 3;
        public int CurRow { get; private set; }
        public int CurCol { get; private set; }

        public fm()
        {
            InitializeComponent();

            ResizeCells();
            DrawCells();
            //this.Paint += Fm_Paint;
            //this.Resize += Fm_Resize;
            //this.MouseMove += Fm_MouseMove;
            pImage.MouseMove += Fm_MouseMove1;
            pImage.Paint += Fm_Paint1;
            pImage.Resize += (s, e) => { ResizeCells(); this.Invalidate(); };

            trackBar1.Minimum = 1;
            trackBar8.Minimum = 1;
            trackBar1.Maximum = 10;
            trackBar8.Maximum = 10;
            trackBar1.Value = Rows;
            trackBar8.Value = Cols;
            trackBar1.ValueChanged += (s, e) => { Rows = trackBar1.Value; ResizeCells(); pImage.Invalidate(); };
            trackBar8.ValueChanged += (s, e) => { Cols = trackBar2.Value; ResizeCells(); pImage.Invalidate(); };
        }

        private void Fm_Paint1(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, 0, 0);
        }

        private void Fm_MouseMove1(object sender, MouseEventArgs e)
        {
            CurRow = e.Y / CellHeigt;
            CurCol = e.X / CellWidth;
            DrawCells();
            pImage.Invalidate();
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            //CurRow = e.Y / CellHeigt;
            //CurCol = e.X / CellWidth;
            for (int i = 0; i < Rows; i++)
            {
                if (i * CellHeigt > e.Y) break;
                CurRow = i;
            }
            for (int i = 0; i < Cols; i++)
            {
                if (i * CellWidth > e.X) break;
                CurCol = i;
            }
            Text = $"({CurRow},{CurCol})";
            DrawCells();
            this.CreateGraphics().DrawImage(b, new Point(0, 0));
        }

        private void Fm_Resize(object sender, EventArgs e)
        {
            ResizeCells();
            DrawCells();
            this.CreateGraphics().DrawImage(b, new Point(0, 0));
        }

        private void Fm_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }

        private void DrawCells()
        {
            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i <= Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), 0, i * CellHeigt, Cols * CellWidth, i * CellHeigt);
                }
                for (int i = 0; i <= Cols; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), i * CellWidth, 0, i * CellWidth, Rows * CellHeigt);
                }
                g.DrawString($"{CurRow},{CurCol}",
                    new Font("", 40),
                    new SolidBrush(Color.Black),
                    CurCol * CellWidth, CurRow * CellHeigt);
                g.DrawRectangle(new Pen(Color.Red, 3), CurCol * CellWidth, CurRow * CellHeigt, CellWidth, CellHeigt);
            }
        }

        private void ResizeCells()
        {
            b = new Bitmap(Width, Height);
            CellWidth = pImage.Width / Cols;
            CellHeigt = pImage.Height / Rows;
            DrawCells();
        }

    }
}
