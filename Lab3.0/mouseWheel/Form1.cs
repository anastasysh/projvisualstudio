﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mouseWheel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            pictureBox1.MouseWheel += mouseWheelpic();
        }
        private MouseEventHandler mouseWheelpic(object sender, MouseEventArgs e)
        {
            int xDelta = e.Delta > 0 ? 2 : -2;
            if (sender is Control x){

                x.Location = new Point(x.Location.X + xDelta * -1, x.Location.Y);
            x.Width += xDelta*2;
            x.Height += xDelta*2;
            }

        }
    }
}
