﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMoveControlOnMouse
{
    public partial class fm : Form
    {
        private Point StartPoint { get; set; }

        public fm()
        {
            InitializeComponent();
            panel1.MouseDown += Panel_MouseDown;
            panel1.MouseMove += Panel_MouseMove;
            panel2.MouseDown += Panel_MouseDown;
            panel2.MouseMove += Panel_MouseMove;
            pictureBox1.MouseDown += Panel_MouseDown;
            pictureBox1.MouseMove += Panel_MouseMove;
        }

        private void Panel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                if (sender is Control)
                {
                    ((Control)sender).Location = PointToClient(x);
                }
            }
        }

        private void Panel_MouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
        }
    }
}
