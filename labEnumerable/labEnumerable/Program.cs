﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labEnumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = Enumerable.Range(3, 7);
            Console.WriteLine(string.Join(", ", x));
            Console.WriteLine($"sum = {x.Sum()}");
            Console.WriteLine();

            var x2 = Enumerable.Repeat("¯\\_(ツ)_/¯", 5);
            Console.WriteLine(string.Join(Environment.NewLine, x2));

            Console.WriteLine();
            var arr = Enumerable.Range(0, 5).ToArray();
            foreach (var item in arr)
            {
                Console.Write(item);
            }

            Console.WriteLine();
            Random rnd = new Random();
            var arr2 = Enumerable.Range(0, 5).OrderBy(_ => rnd.Next()).ToArray();
            foreach (var item in arr2)
            {
                Console.Write(item);
            }

            Console.ReadLine();
        }
    }
}
