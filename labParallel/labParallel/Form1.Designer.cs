﻿namespace labParallel
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edDirTemp = new System.Windows.Forms.TextBox();
            this.buCreateFiles = new System.Windows.Forms.Button();
            this.buDeleteFiles = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.edDirTemp.Location = new System.Drawing.Point(12, 12);
            this.edDirTemp.Name = "textBox1";
            this.edDirTemp.Size = new System.Drawing.Size(636, 22);
            this.edDirTemp.TabIndex = 0;
            // 
            // button1
            // 
            this.buCreateFiles.Location = new System.Drawing.Point(12, 40);
            this.buCreateFiles.Name = "button1";
            this.buCreateFiles.Size = new System.Drawing.Size(218, 23);
            this.buCreateFiles.TabIndex = 1;
            this.buCreateFiles.Text = "Создать N файлов";
            this.buCreateFiles.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.buDeleteFiles.Location = new System.Drawing.Point(236, 40);
            this.buDeleteFiles.Name = "button3";
            this.buDeleteFiles.Size = new System.Drawing.Size(218, 23);
            this.buDeleteFiles.TabIndex = 3;
            this.buDeleteFiles.Text = "Удалить N файлов";
            this.buDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(460, 40);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(188, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 69);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(636, 369);
            this.textBox2.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 450);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buDeleteFiles);
            this.Controls.Add(this.buCreateFiles);
            this.Controls.Add(this.edDirTemp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edDirTemp;
        private System.Windows.Forms.Button buCreateFiles;
        private System.Windows.Forms.Button buDeleteFiles;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox2;
    }
}

