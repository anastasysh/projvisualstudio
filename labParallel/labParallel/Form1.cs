﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labParallel
{
    public partial class Form1 : Form
    {
        private readonly int countFiles = 100;

        public Form1()
        {
            InitializeComponent();

            edDirTemp.Text = Path.Combine(Path.GetTempPath(), Application.ProductName);
            Directory.CreateDirectory(edDirTemp.Text);

            buCreateFiles.Click += BuCreateFiles_Click;
            buDeleteFiles.Click += BuDeleteFiles_Click;
            button4.Click += Button4_Click;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            Parallel.Invoke(
                () => { /*задача 1*/ },
                () => { /*задача 2*/ },
                () => { /*задача 3*/ },
                () => { /*задача 4*/ }
                );
        }

        private void BuDeleteFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, countFiles, (v) => File.Delete(Path.Combine(Path.Combine(edDirTemp.Text, $"файл{v:00}.txt"))));
        }

        private void BuCreateFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, countFiles, (v) => File.Create(Path.Combine(Path.Combine(edDirTemp.Text, $"файл{v:00}.txt"))).Close());
        }
    }
}
