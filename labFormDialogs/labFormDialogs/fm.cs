﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFormDialogs
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            button1.Click += Button1_Click;


        }

        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.InitialDirectory = textBox1.Text;
            openFileDialog.Filter = "txt file (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog.FileName;
            }
            else
            {
                textBox1.Text = "";
            }
        }
    }
}
