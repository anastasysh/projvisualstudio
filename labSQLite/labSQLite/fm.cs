﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSQLite
{
    public partial class fm : Form
    {
        private SQLiteConnection db;

        public fm()
        {
            InitializeComponent();
            //https://www.sqlite.org/download.html
            db = new SQLiteConnection("myDB.db");
            db.CreateTable<Logs>();
            db.CreateTable<Users>();
            db.CreateTable<Notes>();
            
            db.Insert(new Logs() { DT = DateTime.Now });
            lvLogs.View = View.Details;
            lvLogs.Columns.Add("DataTime", 200);
            foreach (var item in db.Table<Logs>())
            {
                lvLogs.Items.Add(item.DT.ToString());
            }
            buUsersShow.Click += (s, e) => dataGridView1.DataSource = db.Table<Users>().ToList();
            buNotesShow.Click += (s, e) => dataGridView1.DataSource = db.Table<Notes>().ToList();
            buNotesAdd.Click += BuNotesAdd_Click;
            buRunSQL.Click += (s, e) => MessageBox.Show(db.ExecuteScalar<int>(edSQL.Text).ToString());
        }

        private void BuNotesAdd_Click(object sender, EventArgs e)
        {
            var x = new Notes();
            x.Note = edNote.Text;
            x.Priority = (byte)edNotePriority.Value;
            db.Insert(x);
            //db.Insert(new Notes() { Note = edNote.Text, Priority = (byte)edNotePriority.Value });
        }

        private class Logs
        {
            public DateTime DT { get; set; }
        }

        private class Users
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string FIO { get; set; }
            public string Email { get; set; }
            public byte age { get; set; }
        }

        private class Notes
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string Note { get; set; }
            public byte Priority { get; set; }
        }
    }
}
