﻿namespace labSQLite
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvLogs = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.edSQL = new System.Windows.Forms.TextBox();
            this.buUsersShow = new System.Windows.Forms.Button();
            this.buNotesShow = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.buRunSQL = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.edNote = new System.Windows.Forms.TextBox();
            this.buNotesAdd = new System.Windows.Forms.Button();
            this.edNotePriority = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotePriority)).BeginInit();
            this.SuspendLayout();
            // 
            // lvLogs
            // 
            this.lvLogs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvLogs.HideSelection = false;
            this.lvLogs.Location = new System.Drawing.Point(12, 29);
            this.lvLogs.Name = "lvLogs";
            this.lvLogs.Size = new System.Drawing.Size(228, 409);
            this.lvLogs.TabIndex = 0;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Лог запусков";
            // 
            // textBox1
            // 
            this.edSQL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edSQL.Location = new System.Drawing.Point(257, 29);
            this.edSQL.Multiline = true;
            this.edSQL.Name = "textBox1";
            this.edSQL.Size = new System.Drawing.Size(531, 72);
            this.edSQL.TabIndex = 2;
            // 
            // buUsersShow
            // 
            this.buUsersShow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buUsersShow.Location = new System.Drawing.Point(257, 107);
            this.buUsersShow.Name = "buUsersShow";
            this.buUsersShow.Size = new System.Drawing.Size(531, 23);
            this.buUsersShow.TabIndex = 3;
            this.buUsersShow.Text = "пользователи";
            this.buUsersShow.UseVisualStyleBackColor = true;
            // 
            // buNotesShow
            // 
            this.buNotesShow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buNotesShow.Location = new System.Drawing.Point(257, 136);
            this.buNotesShow.Name = "buNotesShow";
            this.buNotesShow.Size = new System.Drawing.Size(531, 23);
            this.buNotesShow.TabIndex = 4;
            this.buNotesShow.Text = "заметки";
            this.buNotesShow.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(257, 165);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(263, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Выполнить1";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.buRunSQL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buRunSQL.Location = new System.Drawing.Point(526, 165);
            this.buRunSQL.Name = "button4";
            this.buRunSQL.Size = new System.Drawing.Size(262, 23);
            this.buRunSQL.TabIndex = 6;
            this.buRunSQL.Text = "Выполнить SQL";
            this.buRunSQL.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(257, 194);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(531, 215);
            this.dataGridView1.TabIndex = 7;
            // 
            // textBox2
            // 
            this.edNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edNote.Location = new System.Drawing.Point(257, 416);
            this.edNote.Name = "textBox2";
            this.edNote.Size = new System.Drawing.Size(335, 22);
            this.edNote.TabIndex = 8;
            // 
            // button5
            // 
            this.buNotesAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buNotesAdd.Location = new System.Drawing.Point(700, 415);
            this.buNotesAdd.Name = "button5";
            this.buNotesAdd.Size = new System.Drawing.Size(88, 23);
            this.buNotesAdd.TabIndex = 9;
            this.buNotesAdd.Text = "Добавить";
            this.buNotesAdd.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.edNotePriority.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.edNotePriority.Location = new System.Drawing.Point(598, 416);
            this.edNotePriority.Name = "numericUpDown1";
            this.edNotePriority.Size = new System.Drawing.Size(96, 22);
            this.edNotePriority.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(254, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "SQL";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edNotePriority);
            this.Controls.Add(this.buNotesAdd);
            this.Controls.Add(this.edNote);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buRunSQL);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buNotesShow);
            this.Controls.Add(this.buUsersShow);
            this.Controls.Add(this.edSQL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvLogs);
            this.Name = "fm";
            this.Text = "SQLite";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotePriority)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvLogs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edSQL;
        private System.Windows.Forms.Button buUsersShow;
        private System.Windows.Forms.Button buNotesShow;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buRunSQL;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox edNote;
        private System.Windows.Forms.Button buNotesAdd;
        private System.Windows.Forms.NumericUpDown edNotePriority;
        private System.Windows.Forms.Label label2;
    }
}

