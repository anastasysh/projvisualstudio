﻿namespace labSoundPlayer
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.buSysSound = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buPlay
            // 
            this.buPlay.Location = new System.Drawing.Point(12, 83);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(75, 23);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "Play";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Location = new System.Drawing.Point(93, 83);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(75, 23);
            this.buStop.TabIndex = 1;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(174, 83);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Loop";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // buSysSound
            // 
            this.buSysSound.Location = new System.Drawing.Point(255, 83);
            this.buSysSound.Name = "buSysSound";
            this.buSysSound.Size = new System.Drawing.Size(75, 23);
            this.buSysSound.TabIndex = 3;
            this.buSysSound.Text = "SysSignal";
            this.buSysSound.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buSysSound);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPlay);
            this.Name = "fm";
            this.Text = "labSoundPlayer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buSysSound;
    }
}

