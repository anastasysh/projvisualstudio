﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\temp.txt";
            var list = new List<string>
            {
                "¯\_(ツ)_/¯",
                "¯\_(ツ)_/¯",
                "¯\_(ツ)_/¯"
            };
            var xWriteText = string.Join(Environment.NewLine, list);
            File.WriteAllText(path, xWriteText);

            var xReadText = File.ReadAllText(path);
            Console.WriteLine(xReadText);
            Console.WriteLine();

            Console.WriteLine(File.Exists(path));
            Console.WriteLine();


            File.Delete(path);

            Console.ReadLine();
        }
    }
}
