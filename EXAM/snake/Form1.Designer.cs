﻿namespace snakeExam
{
    partial class mainMenu
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.newGame = new System.Windows.Forms.Button();
            this.selectLvl = new System.Windows.Forms.Button();
            this.tableOfRecords = new System.Windows.Forms.Button();
            this.customLvl = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newGame
            // 
            this.newGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.newGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newGame.ForeColor = System.Drawing.SystemColors.ControlText;
            this.newGame.Location = new System.Drawing.Point(64, 57);
            this.newGame.Name = "newGame";
            this.newGame.Size = new System.Drawing.Size(334, 59);
            this.newGame.TabIndex = 0;
            this.newGame.Text = "Начать игру";
            this.newGame.UseVisualStyleBackColor = false;
            this.newGame.Click += new System.EventHandler(this.newGameFunc);
            // 
            // selectLvl
            // 
            this.selectLvl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.selectLvl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectLvl.Location = new System.Drawing.Point(64, 145);
            this.selectLvl.Name = "selectLvl";
            this.selectLvl.Size = new System.Drawing.Size(334, 59);
            this.selectLvl.TabIndex = 1;
            this.selectLvl.Text = "Выбор уровня";
            this.selectLvl.UseVisualStyleBackColor = false;
            this.selectLvl.Click += new System.EventHandler(this.SelectLvlFunc);
            // 
            // tableOfRecords
            // 
            this.tableOfRecords.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tableOfRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tableOfRecords.Location = new System.Drawing.Point(64, 237);
            this.tableOfRecords.Name = "tableOfRecords";
            this.tableOfRecords.Size = new System.Drawing.Size(334, 59);
            this.tableOfRecords.TabIndex = 2;
            this.tableOfRecords.Text = "Зал славы ";
            this.tableOfRecords.UseVisualStyleBackColor = false;
            this.tableOfRecords.Click += new System.EventHandler(this.tableOfRecordsFunc);
            // 
            // customLvl
            // 
            this.customLvl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.customLvl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.customLvl.Location = new System.Drawing.Point(64, 333);
            this.customLvl.Name = "customLvl";
            this.customLvl.Size = new System.Drawing.Size(334, 59);
            this.customLvl.TabIndex = 3;
            this.customLvl.Text = "Редактор карт";
            this.customLvl.UseVisualStyleBackColor = false;
            this.customLvl.Click += new System.EventHandler(this.customLvlFunc);
            // 
            // mainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 470);
            this.Controls.Add(this.customLvl);
            this.Controls.Add(this.tableOfRecords);
            this.Controls.Add(this.selectLvl);
            this.Controls.Add(this.newGame);
            this.Name = "mainMenu";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button newGame;
        private System.Windows.Forms.Button selectLvl;
        private System.Windows.Forms.Button tableOfRecords;
        private System.Windows.Forms.Button customLvl;
    }
}

