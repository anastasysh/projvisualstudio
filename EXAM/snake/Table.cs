﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeExam
{
    public partial class Table : Form
    {
        public int Count;
        public int maxCount;
        DB db = new DB();
        public Table()
        {
            InitializeComponent();
            init();
        }

        private void init() {
            Count = 1;
            maxCount = db.getMaxCount();
            createTable();
        }
        private void createTable() {
            dataGridView1.Rows.Clear(); 
            List<string[]> top = new List<string[]>();
            top = db.top10(Count);
            foreach (string[] rec in top) {
                dataGridView1.Rows.Add(rec);
            }
        }
        private void rightClick(object sender, EventArgs e)
        {
            if (Count < maxCount)
            {
                Count += 1;
            }
            else
            {
                Count = 1;
            }
            numLVL.Text = Count.ToString();
            createTable();
        }

        private void leftClick(object sender, EventArgs e)
        {
            if (Count > 1)
            {
                Count -= 1;
            }
            else
            {
                Count = maxCount;
            }
            numLVL.Text = Count.ToString();
            createTable();
        }
    }
}
