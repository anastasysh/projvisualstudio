﻿using snakeExam.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeExam
{
    public partial class menu : Form
    {
        private Bitmap boardBitm;// битмап для самого поля
        private Bitmap menuBitm; //битмап для меню
        public bool isPressed = false;
        private Dictionary<char, Rectangle> pics = new Dictionary<char, Rectangle>(); //координаты вырезания объекта
        private Dictionary<int, char> bind = new Dictionary<int, char>(); // координаты вырезания
        char[] ims = { '5', '6' };
        string path = @".\dict.txt";
        Image img = Resources.mapElement2; //изображение карты
        public int Rows { get; private set; } = 30;
        public int Cols { get; private set; } = 30;
        public int xCW;//ширина ячейки
        public int xCH; // высота ячейки
        public int selected = 9;
        private Point StartPoint;
        public Dictionary<int[], char> dict = new Dictionary<int[], char>();  //хранит уже сделанное на карте
        public menu()
        {
            InitializeComponent();
            DictFill();
            DrawCells();
            DrawMenu();
            save.Click += Bsave_Click;
            pboard.Paint += Pboard_Paint;
            pmenu.Paint += Pmenu_Paint;
            pmenu.MouseUp += Pmenu_MouseUp;
            pboard.MouseUp += Pboard_MouseUp;
            pboard.MouseMove += Pboard_MouseMove;
            pboard.MouseDown += Pboard_MouseDown;
        }
        private void Pboard_MouseDown(object sender, MouseEventArgs e) //нажатие мышки
        {
            StartPoint = new Point(e.X, e.Y);
            isPressed = true;
        }

        private void Pboard_MouseMove(object sender, MouseEventArgs e) //передвижение мышки
        {
            if (e.Button == MouseButtons.Right)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                if (sender is Control)
                {
                    ((Control)sender).Location = PointToClient(x);
                }
            }
            if (e.Button == MouseButtons.Left)
            {
                if (isPressed)
                {
                    Pboard_MouseUp(sender, e);
                };
            }
        }

        private void Bload_Click(object sender, EventArgs e) //загрузка карты
        {
            dict.Clear(); //очистка словаря
            using (var reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var a = line.Split('\t');
                    int[] arr = { int.Parse(a[0]), int.Parse(a[1]) };
                    dict.Add(arr, char.Parse(a[2]));
                }
            }
            DrawCells();
        }

        private void Bsave_Click(object sender, EventArgs e) //сохранение карты
        {
            String tmpMap = "";
            using (var writer = new StreamWriter(path))
            { 
                foreach (var kvp in dict)
                {
                    tmpMap+= $"{kvp.Key[0]},{kvp.Key[1]},{kvp.Value};";
                }
            }
            {
                DB db = new DB();
                db.createTable();
                db.addLVL(tmpMap);
                label1.Text = "карта сохранена";
                timer1.Interval = 10;
                timer1.Tick += disabled;
            }
    

        }

        private void disabled(object sender, EventArgs e)
        {
            label1.Text = "";
            timer1.Stop();
        }

        private void Pboard_MouseUp(object sender, MouseEventArgs e) //отпускание мышки над полем
        {
            if (selected != 9 && e.Button == MouseButtons.Left) //если что-то выбрано и отпущена левая кнопка
            {
                int x, y;
                x = e.X / (pboard.Width / Cols); //поиск колонки
                y = e.Y / (pboard.Height / Rows); //поиск строки

                using (var g = Graphics.FromImage(boardBitm))
                {
                    g.Clear(DefaultBackColor);
                    g.DrawImage(img,
                            new Rectangle(xCW * x, xCH * y, xCW, xCH),
                            pics[bind[selected]], //это должны быть координаты картинки pics это координаты вырезания  bind это 2 числа и буква
                            GraphicsUnit.Pixel);
                    this.Refresh();
                    PaintEventArgs s = new PaintEventArgs(g, new Rectangle(0, 0, pboard.Width, pboard.Height));
                    Pboard_Paint(pboard, s);
                }
                int[] arr = { x, y };
                dict.Add(arr, bind[selected]);
                DrawCells();
            }
        }

        private void Pmenu_MouseUp(object sender, MouseEventArgs e)  //отпускание мышки над меню
        {
            int x;
            if (e.X < pmenu.Width / 2)
                x = 0;
            else
                x = 1;
            selected = x ;
            System.Console.WriteLine($"{selected}");

        }

        private void Pmenu_Paint(object sender, PaintEventArgs e) 
        {
            e.Graphics.DrawImage(menuBitm, 0, 0); 
        }

        private void Pboard_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(boardBitm, 0, 0);
        }

        private void DrawMenu() //отрисовка меню
        {
            menuBitm = new Bitmap(pmenu.Width, pmenu.Height);
            using (var g = Graphics.FromImage(menuBitm))
            {
                g.Clear(DefaultBackColor);
                        g.DrawImage(img,
                        new Rectangle(new Point(0, 0), new Size(pmenu.Width / 2, pmenu.Height)),
                        pics[ims[0]], //???
                        GraphicsUnit.Pixel);
                g.DrawImage(img,
                        new Rectangle(new Point(pmenu.Width / 2 + 10, 0), new Size(pmenu.Width / 2, pmenu.Height)),
                        pics[ims[1]], //???
                        GraphicsUnit.Pixel);                       
      
            }
        }

        private void DrawCells() //отрисовка сетки
        {
            boardBitm = new Bitmap(pboard.Height, pboard.Width);
            using (var g = Graphics.FromImage(boardBitm))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i <= Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), 0, i * xCH, Cols * xCW, i * xCH);
                }
                for (int i = 0; i <= Cols; i++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), i * xCW, 0, i * xCW, Rows * xCH);
                }
                g.DrawLine(new Pen(Color.Black, 1), 0, Cols * xCH - 1, Cols * xCW, Cols * xCH - 1);
                g.DrawLine(new Pen(Color.Black, 1), Rows * xCW - 1, 0, Cols * xCW - 1, Rows * xCH);

                foreach (KeyValuePair<int[], char> kvp in dict)
                {
                    g.DrawImage(img,
                            new Rectangle(xCW * kvp.Key[0], xCH * kvp.Key[1], xCW, xCH),
                            pics[kvp.Value],
                            GraphicsUnit.Pixel);
                }

                pboard.Refresh();

  

        }
        }

        private void DictFill() //инициализация основных переменных
        {
            
            pboard.Height = 450; //размер поля 
            pboard.Width = 450;
            xCW = pboard.Width / Cols;
            xCH = pboard.Height / Rows;
            img = ResizeImage(img, pboard.Width, pboard.Height);
            int k = 0;
            for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Rectangle tmp;
                    tmp = new Rectangle(j *(pboard.Width / 2), i * pboard.Height, pboard.Width / 2, pboard.Height);
                    pics.Add(ims[k], tmp); 
                    bind.Add(k, ims[k]);
                    k++;
                }
            }
        }

        private Image ResizeImage(Image img, int newWidth, int newHeight) //переразмер карты
        {
            Image thumbImg = new Bitmap(newWidth, newHeight);

            using (Graphics gr = Graphics.FromImage(thumbImg))
            {
                gr.DrawImage(img, new Rectangle(0, 0, newWidth, newHeight), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            }
            return thumbImg;
        }


    }


}
