﻿namespace snakeExam
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bsave = new System.Windows.Forms.Button();
            this.pmenu = new System.Windows.Forms.Panel();
            this.pboard = new System.Windows.Forms.Panel();
            this.save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // bsave
            // 
            this.bsave.Location = new System.Drawing.Point(37, 595);
            this.bsave.Name = "bsave";
            this.bsave.Size = new System.Drawing.Size(200, 40);
            this.bsave.TabIndex = 1;
            this.bsave.Text = "Cохранить";
            this.bsave.UseVisualStyleBackColor = true;
            // 
            // pmenu
            // 
            this.pmenu.Location = new System.Drawing.Point(12, 12);
            this.pmenu.Name = "pmenu";
            this.pmenu.Size = new System.Drawing.Size(334, 100);
            this.pmenu.TabIndex = 2;
            // 
            // pboard
            // 
            this.pboard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pboard.Location = new System.Drawing.Point(402, 12);
            this.pboard.Name = "pboard";
            this.pboard.Size = new System.Drawing.Size(1468, 971);
            this.pboard.TabIndex = 3;
            // 
            // save
            // 
            this.save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.save.Location = new System.Drawing.Point(81, 155);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(200, 39);
            this.save.TabIndex = 4;
            this.save.Text = "Сохранить";
            this.save.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 246);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 5;
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 1050);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.save);
            this.Controls.Add(this.pboard);
            this.Controls.Add(this.pmenu);
            this.Name = "menu";
            this.Text = "Custom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bsave;
        private System.Windows.Forms.Panel pmenu;
        private System.Windows.Forms.Panel pboard;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
    }
}