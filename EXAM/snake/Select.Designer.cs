﻿namespace snakeExam
{
    partial class Select
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.left = new System.Windows.Forms.Button();
            this.right = new System.Windows.Forms.Button();
            this.pBoard = new System.Windows.Forms.Panel();
            this.numLVL = new System.Windows.Forms.Label();
            this.go = new System.Windows.Forms.Button();
            this.multiplayer = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // left
            // 
            this.left.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.left.Location = new System.Drawing.Point(62, 511);
            this.left.Name = "left";
            this.left.Size = new System.Drawing.Size(107, 35);
            this.left.TabIndex = 0;
            this.left.Text = "тудысь";
            this.left.UseVisualStyleBackColor = false;
            this.left.Click += new System.EventHandler(this.LeftClick);
            // 
            // right
            // 
            this.right.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.right.Location = new System.Drawing.Point(256, 511);
            this.right.Name = "right";
            this.right.Size = new System.Drawing.Size(128, 35);
            this.right.TabIndex = 1;
            this.right.Text = "сюдысь";
            this.right.UseVisualStyleBackColor = false;
            this.right.Click += new System.EventHandler(this.rightClick);
            // 
            // pBoard
            // 
            this.pBoard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pBoard.Location = new System.Drawing.Point(139, 50);
            this.pBoard.Name = "pBoard";
            this.pBoard.Size = new System.Drawing.Size(456, 398);
            this.pBoard.TabIndex = 2;
            this.pBoard.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintHandler);
            // 
            // numLVL
            // 
            this.numLVL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numLVL.AutoSize = true;
            this.numLVL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numLVL.Location = new System.Drawing.Point(199, 518);
            this.numLVL.Name = "numLVL";
            this.numLVL.Size = new System.Drawing.Size(19, 20);
            this.numLVL.TabIndex = 3;
            this.numLVL.Text = "1";
            // 
            // go
            // 
            this.go.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.go.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.go.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.go.Location = new System.Drawing.Point(473, 511);
            this.go.Name = "go";
            this.go.Size = new System.Drawing.Size(159, 35);
            this.go.TabIndex = 4;
            this.go.Text = "Начать игру =>";
            this.go.UseVisualStyleBackColor = false;
            this.go.Click += new System.EventHandler(this.goClick);
            // 
            // multiplayer
            // 
            this.multiplayer.AutoSize = true;
            this.multiplayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.multiplayer.Location = new System.Drawing.Point(473, 565);
            this.multiplayer.Name = "multiplayer";
            this.multiplayer.Size = new System.Drawing.Size(119, 24);
            this.multiplayer.TabIndex = 5;
            this.multiplayer.Text = "два игрока";
            this.multiplayer.UseVisualStyleBackColor = true;
            // 
            // Select
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 606);
            this.Controls.Add(this.multiplayer);
            this.Controls.Add(this.go);
            this.Controls.Add(this.numLVL);
            this.Controls.Add(this.pBoard);
            this.Controls.Add(this.right);
            this.Controls.Add(this.left);
            this.DoubleBuffered = true;
            this.Name = "Select";
            this.Text = "Select";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button left;
        private System.Windows.Forms.Button right;
        private System.Windows.Forms.Panel pBoard;
        private System.Windows.Forms.Label numLVL;
        private System.Windows.Forms.Button go;
        private System.Windows.Forms.CheckBox multiplayer;
    }
}