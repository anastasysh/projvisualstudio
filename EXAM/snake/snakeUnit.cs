﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snakeExam
{
    public class snakeUnit
    {
        public Point[] snake = new Point[288];
        public int score;
        public int snakeCount;
        public int dirSnakeX = 0; // сдвиг змеи тудысь
        public int dirSnakeY = 0; // сдвиг змеи сюдысь
        public int head = 0;
        public int body = 0;


        public snakeUnit(int X, int Y, int head, int body, int dirX, int dirY)
        {
            score = 0;
            snakeCount = 1;
            dirSnakeX = dirX;
            dirSnakeY = dirY;
            snake[0].X = X;
            snake[0].Y = Y;
            this.head = head;
            this.body = body;
            


        }

        public void clear() {
            this.snakeCount = 0;
            this.score = 0;
            this.head = 0;
            this.body = 0;
        }
    }
}
