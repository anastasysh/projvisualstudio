﻿using Microsoft.Data.Sqlite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snakeExam
{
    class DB
    {
        public SQLiteConnection db = new SQLiteConnection("myDB.db");

        public void createTable()
        {
            db.CreateTable<LVLs>();
            db.CreateTable<Logs>();
            Logs test = new Logs();
            test.Name = "робот-пылесос";
            test.Score = "150";
            test.LvlID = "1";
            db.Insert(test);
        }
        public void addLVL(String map) {
            LVLs x = new LVLs();
            x.Map = map;
            db.Insert(x);
        }
        public String getLVL(int numLVL) {

            List<LVLs> map = db.Query<LVLs>($"SELECT * FROM 'LVLs' WHERE  ID ={numLVL}");
            return (map[0].Map);
        }

        public int getMaxCount() {
            List<LVLs> all = db.Query<LVLs>($"SELECT * FROM 'LVLs'");
            return (all.Count);
        }

        public void setResult(int score, String name, int LVL)
        {
            Logs rec = new Logs();
            rec.Score = score.ToString();
            rec.Name = name;
            rec.LvlID = LVL.ToString();
            db.Insert(rec);
        }

        public List<string[]> top10(int LVL) {
            List <Logs> rate = db.Query<Logs>($"SELECT * FROM 'Logs' WHERE LvlID={LVL} ORDER BY Score DESC LIMIT 10");
            List<string[]> ret = new List<string[]>();

            for (int i = 0; i < rate.Count; i++) {
                ret.Add(new string[2]);
                ret[i][0] = rate[i].Name;
                ret[i][1]= rate[i].Score;
                }
            return (ret);


        }

        private class LVLs
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string Map { get; set; }
        }
        private class Logs
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string Name { get; set; }
            public string Score { get; set; }
            public string LvlID { get; set; }
        }
    }
}
