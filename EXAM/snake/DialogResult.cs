﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeExam
{
    public partial class DialogResult : Form
    {
        public int score;
        public int LVL;
        public DialogResult()
        {
            InitializeComponent();
            scoreLabel.Text = "";
        }

        private void saveRes(object sender, EventArgs e)
        {
            String name = textBox1.Text;
            if (name != "")
            {
                DB db = new DB();
                db.setResult(score, name, LVL);
                this.Close();
            }
        }
    }
}
