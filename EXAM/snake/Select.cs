﻿using snakeExam.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeExam
{
    public partial class Select : Form
    {
        public int Count; //текущий счетчик уровня
        public int maxCount; //максимальное количество уровней
        public int sizeCell = 8;
        const int mapWidth = 30;
        const int mapHeight = 30;
        public Image rockSet;
        public Image waterSet;
        public Image grassSet;

        public int[,] map = new int[mapWidth, mapHeight];
        public Select()
        {
            InitializeComponent();
            init();
        }
        public void init() {
            Count = 2;
            DB db = new DB();
            maxCount = db.getMaxCount();
            LoadMap(Count);
            grassSet = new Bitmap(Resources.maxresdefault);
            rockSet = new Bitmap(Resources.kisspng_pixel_art_sprite_rocks_5abdc79be055b5_6774491615223868439189);
            waterSet = new Bitmap(Resources.vUHmzP);
        }

        private void LoadMap(int Count) {
            DB db = new DB();
            String tmpMap = db.getLVL(Count);
            CreateMap(tmpMap);
            pBoard.Invalidate();
        }
        private void CreateMap(String tmpMap) //парс образа карты из БД и составление нормальной 
        {
            String [] allRowElementsInTmpMap = tmpMap.Split(';'); 
            for (int i = 0; i < 30; i++) {
                for (int j = 0; j < 30; j++)
                {
                    map[i, j] = 0; 
                }
            }
            for (int i = 0; i < allRowElementsInTmpMap.Length-1; i++) {
                String[] allColElementsInTmpMap = allRowElementsInTmpMap[i].Split(','); //0-x 1-y 2 - препятствие
                map[int.Parse(allColElementsInTmpMap[0]), int.Parse(allColElementsInTmpMap[1])] = int.Parse(allColElementsInTmpMap[2]);
            }
        }

        private void paintMap(Graphics g) {
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if (map[i, j] == 5) // числа камней
                    {
                        g.DrawImage(rockSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 0, 0, 712, 493, GraphicsUnit.Pixel);
                    }
                    if (map[i, j] == 6) // числа воды
                    {
                        g.DrawImage(waterSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 0, 0, 580, 580, GraphicsUnit.Pixel);
                    }
                }
            }
        }
        private void LeftClick(object sender, EventArgs e)
        {
            if (Count > 1)
            {
                Count -= 1;
            }
            else {
                Count = maxCount;
            }
            numLVL.Text = Count.ToString();
            LoadMap(Count);
        }

        private void rightClick(object sender, EventArgs e)
        {
            if (Count < maxCount)
            {
                Count += 1;
            }
            else
            {
                Count = 1;
            }
            numLVL.Text = Count.ToString();
            LoadMap(Count);
        }

        private void goClick(object sender, EventArgs e)
        {
            Game game = new Game();
            for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    game.referenceMap[i, j] = 0;
                }
            }
                    for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    game.referenceMap[i, j] = map[i, j]; //не переносится последнее
                }
            }

            game.LVL = Count;
            game.multipleer = multiplayer.Checked;
            game.ShowDialog();

        }

        public void drawArea(Graphics g) //отрисовка контура поля
        {
            g.DrawRectangle(Pens.Black, new Rectangle(0, 0, mapWidth * sizeCell, mapHeight * sizeCell));

        }
        private void PaintHandler(object sender, PaintEventArgs e)
        {
            paintMap(e.Graphics);
            drawArea(e.Graphics);
        }
    }
}
