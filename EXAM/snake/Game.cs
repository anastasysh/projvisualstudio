﻿using snakeExam.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snakeExam
{
    public partial class Game : Form
    {
        public int[,] referenceMap = new int[mapWidth, mapHeight];
        public bool multipleer;
        const int mapWidth = 30;
        const int mapHeight = 30;
        public int[,] map = new int[mapWidth, mapHeight]; //общая карта
        const int sizeCell = 10; // размер ячейки
        public int snakeSpeed; //скорость змеи
        public int elementRange = 10000; // интервал появления предметов
        
        public Image snakeSet; // спрайты змеи
        public Image cherry; //спрайт вишенки
        public Image foodSet; //спрайт мусора
        public Image rockSet; //спрайт препятствий
        public Image waterSet;
      
        public bool checkFruit; // флаг фруктов
        public bool checkTrash; // флаг мусора
        public int appleX;
        public int appleY;
        public int trashX;
        public int trashY;

        public Label scoreLabel; // очки змеи 1
        public Label scoreLabel2; // очки змеи 2
        snakeUnit Collidesnake;
        public int LVL;

        snakeUnit snake1;
        snakeUnit snake2;
        public Game()
        {
            InitializeComponent();
            
            scoreLabel = new Label();
            scoreLabel.Location = new Point(sizeCell, (mapHeight * sizeCell) + sizeCell);
            scoreLabel.Text = "Score: 0";

            scoreLabel2 = new Label();
            scoreLabel2.Location = new Point(sizeCell, (mapHeight * sizeCell) + sizeCell * 2);
            scoreLabel2.Text = "Score: 0";

            this.KeyDown += new KeyEventHandler(pressKey);
            gameSpeed.Tick += new EventHandler(update);
            element.Tick += new EventHandler(createCherry);
        }

        private void update(object sender, EventArgs e)
        {
            if (!isCollide())
            {
                eatFruit();
                updateSnake(snake1);
                if (multipleer)
                {
                    updateSnake(snake2);
                }
                Invalidate();
            }
            else
            {
                result();
            }
        }
        private void updateSnake(snakeUnit snake) {
            map[snake.snake[snake.snakeCount - 1].X, snake.snake[snake.snakeCount - 1].Y] = 0;
            for (int i = snake.snakeCount - 1; i >= 1; i--)
            {
                snake.snake[i].X = snake.snake[i - 1].X;
                snake.snake[i].Y = snake.snake[i - 1].Y;
                map[snake.snake[i].X, snake.snake[i].Y] = snake.body;
            }

            snake.snake[0].X += snake.dirSnakeX;
            snake.snake[0].Y += snake.dirSnakeY;
            map[snake.snake[0].X, snake.snake[0].Y] = snake.head;
        }

        public bool isCollide() //сверка коллизий c границами поля
        {
            bool collide = false;
            collide = isCollideSnake(snake1);
            if (collide) { return collide; };
            if (multipleer)
            {
                collide = isCollideSnake(snake2);
                if (collide) { return collide; };
                collide = isCrossCollideSnake(snake1, snake2);
                if (collide) { return collide; };
            }
            return collide;
        }
        public bool isCrossCollideSnake(snakeUnit snake, snakeUnit snake2) {
            bool collide = false;
            if (map[snake2.snake[0].X + snake2.dirSnakeX, snake2.snake[0].Y + snake2.dirSnakeY] == snake1.body
                        || map[snake.snake[0].X + snake.dirSnakeX, snake.snake[0].Y + snake.dirSnakeY]== snake.body
                        || snake2.snake[0].X +snake2.dirSnakeX == snake.snake[0].X && snake2.snake[0].Y +snake2.dirSnakeX == snake.snake[0].Y
                        || snake2.snake[0].X == snake.snake[0].X && snake2.snake[0].Y == snake.snake[0].Y)
            {
                collide = true;
                Collidesnake = snake;
                return collide;
            }
            return collide;
        }
        public bool isCollideSnake(snakeUnit snake)
        {
            bool collide = false;
            if (snake.snake[0].X + snake.dirSnakeX < 0 || snake.snake[0].X + snake.dirSnakeX > mapWidth - 1)
            {
                collide = true;
                Collidesnake = snake;
                return collide;

            };
            if (snake.snake[0].Y + snake.dirSnakeY < 0 || snake.snake[0].Y + snake.dirSnakeY > mapWidth - 1)
            {
                collide = true;
                Collidesnake = snake;
                return collide;
            }
            if (map[snake.snake[0].X + snake.dirSnakeX, snake.snake[0].Y + snake.dirSnakeY] == snake.body
                || map[snake.snake[0].X + snake.dirSnakeX, snake.snake[0].Y + snake.dirSnakeY] == 5
                || map[snake.snake[0].X + snake.dirSnakeX, snake.snake[0].Y + snake.dirSnakeY] == 6) //коллизия со стенками и телом
            {
                collide = true;
                Collidesnake = snake;
                return collide;
            }
            
            return collide;
        }
        public void eatFruit() // поедание фруктов
        {
            eatFruitSnake(snake1);
            if (multipleer)
            {
            eatFruitSnake(snake2);
            }
            
        }
        public void eatFruitSnake(snakeUnit snake)
        {
            if (snake.snake[0].X == appleX && snake.snake[0].Y == appleY)
            {
                if (checkFruit)
                {
                    snake.score += 50;
                    scoreLabel.Text = "Score: " + snake.score;
                    snake.snakeCount++;
                    snake.snake[snake.snakeCount - 1].X = snake.snake[snake.snakeCount - 2].X + snake.dirSnakeX;
                    snake.snake[snake.snakeCount - 1].Y = snake.snake[snake.snakeCount - 2].Y - snake.dirSnakeY;
                    map[snake.snake[snake.snakeCount - 1].X, snake.snake[snake.snakeCount - 1].Y] = snake.body;
                    checkFruit = false;
                    checkTrash = false;
                }
                else if (checkTrash)
                {
                    snake.score -= 50;
                    scoreLabel.Text = "Score: " + snake.score;
                    checkTrash = false;
                    checkFruit = false;
                }
                createCherry();
            }
        }
        private void pressKey(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode.ToString())
            {

                case "Right":
                    snake1.dirSnakeX = 1;
                    snake1.dirSnakeY = 0;
                    break;
                case "Left":
                    snake1.dirSnakeX = -1;
                    snake1.dirSnakeY = 0;
                    break;
                case "Up":
                    snake1.dirSnakeY = -1;
                    snake1.dirSnakeX = 0;
                    break;
                case "Down":
                    snake1.dirSnakeY = 1;
                    snake1.dirSnakeX = 0;
                    break;
            }
            if (multipleer)
            {
                switch (e.KeyCode.ToString())
                {

                    case "D":
                        snake2.dirSnakeX = 1;
                        snake2.dirSnakeY = 0;

                        break;
                    case "A":
                        snake2.dirSnakeX = -1;
                        snake2.dirSnakeY = 0;
                        break;
                    case "W":
                        snake2.dirSnakeY = -1;
                        snake2.dirSnakeX = 0;
                        break;
                    case "S":
                        snake2.dirSnakeY = 1;
                        snake2.dirSnakeX = 0;
                        break;
                }
            }
        }
        private void paint(object sender, PaintEventArgs e) //запуск отрисовки поля
        {
            drawMap(e.Graphics);
            drawArea(e.Graphics);
        }
        public void init() // инициализация игровых элементов
        { 
            this.Width = mapWidth * sizeCell + 18;
            this.Height = mapHeight * sizeCell + sizeCell * 10;
            snakeSpeed = 150;
            

            snake1 = new snakeUnit(mapWidth / 2, mapHeight / 2, 1, 4, -1 , 0);
        
            if (multipleer)
            {
                snake2 = new snakeUnit((mapWidth / 2)+4, (mapHeight / 2), 10, 40, 1, 0);
            }

            for (int i = 0; i < mapHeight; i++) {
                for (int j = 0; j < mapWidth; j++) {
                    map[i, j] = referenceMap[i, j];
                }
            }

            snakeSet = new Bitmap(Resources.Безымянный);
            cherry = new Bitmap(Resources._32190c3675f3294);
            foodSet = new Bitmap(Resources.kisspng_computer_icons_pixel_art_clip_art_food_background_5ad7e224cc3301_2943783815240975728364);
            waterSet = new Bitmap(Resources.vUHmzP);

            gameSpeed.Interval = snakeSpeed;
            element.Interval = elementRange;
            checkFruit = false;
            checkTrash = false;

            createCherry();

            this.Controls.Add(scoreLabel);
            this.Controls.Add(scoreLabel2);

            System.Windows.Forms.DialogResult res = MessageBox.Show("Продолжить?", "Пауза",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (res == System.Windows.Forms.DialogResult.Yes)
            {
                gameSpeed.Start();
                element.Start();
            }
            else {
                this.Close();
            }

        }
        public void createCherry(object sender, EventArgs e)   //создание еды или мусора по таймеру. перегрузка функции
        {
            map[appleX, appleY] = 0;
            Random rnd = new Random();

            startRnd:
            appleX = rnd.Next(30);
            appleY = rnd.Next(30);

            if (map[appleX, appleY] != 0)
            {
                goto startRnd;
            }
            map[appleX, appleY] = rnd.Next(2, 4);
            

            if (map[appleX, appleY] == 2)
            {
                checkFruit = true;
                checkTrash = false;
            }
            else if (map[appleX, appleY] == 3)
            {
                checkTrash = true;
                checkFruit = false;
            }
        }
        public void createCherry() //создание еды или мусора
        { 
            startRnd:
            map[appleX, appleY] = 0;
            Random rnd = new Random();
            appleX = rnd.Next(30);
            appleY = rnd.Next(30);
            if (map[appleX, appleY] != 0) {
                goto startRnd;
            }
            map[appleX, appleY] = rnd.Next(2,4);
            if (map[appleX, appleY] == 2)
            {
                checkFruit = true;
                checkTrash = false;
            }
            else if (map[appleX, appleY] == 3) {
                checkTrash = true;
                checkFruit = false;
            }
        }
        
        public void drawMap(Graphics g) //отрисовка элементов карты
        {
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if (map[i, j] == snake1.head) 
                    {
                        
                        g.DrawImage(snakeSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 0, 0, 55,55, GraphicsUnit.Pixel); 
                    }
                    if (map[i, j] == 2) //числа вишенок
                    {
                        g.DrawImage(cherry, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 0, 0, 1700, 2000, GraphicsUnit.Pixel); 
                    }
                    if (map[i, j] == 3) //числа мусора
                    {
                        g.DrawImage(foodSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 221, 410, 171, 112, GraphicsUnit.Pixel); 
                    }
                    if (map[i, j] == snake1.body) //числа тела змеи
                    {
                        g.DrawImage(snakeSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 0, 62, 55, 55, GraphicsUnit.Pixel);
                    }
                    if (multipleer)
                    {
                        if (map[i, j] == snake2.head) //числа головы змеи2
                        {
                            g.DrawImage(snakeSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 64, 0, 55, 55, GraphicsUnit.Pixel);
                        }
                        if (map[i, j] == snake2.body) //числа тела змеи2
                        {
                            g.DrawImage(snakeSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 64, 62, 55, 55, GraphicsUnit.Pixel);
                        }
                    }
                    if (map[i, j] == 5) //числа камней
                    {
                        g.DrawImage(snakeSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 134, 62, 55, 55, GraphicsUnit.Pixel);
                    }
                    if (map[i, j] == 6) // числа воды
                    {
                        g.DrawImage(waterSet, new Rectangle(new Point(i * sizeCell, j * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 0, 0, 580, 580, GraphicsUnit.Pixel);
                    }
                }
            }
        }
        public void drawArea(Graphics g) //отрисовка контура поля
        {
            g.DrawRectangle(Pens.Black, new Rectangle(0, 0, mapWidth * sizeCell, mapHeight * sizeCell));

        }

        public void result() {
            if (LVL == 0)
            {
                gameSpeed.Stop();
                element.Stop();
                MessageBox.Show("Учет очков возможен только при выборе уровня", "Внимание",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
               
            init();
            }
            else {
                gameSpeed.Stop();
                element.Stop();
                DialogResult res = new DialogResult();
                res.score = Collidesnake.score;
                Collidesnake.clear();
                res.LVL = LVL;
                res.ShowDialog();
                init();
            }
        }
        private void ShowInit(object sender, EventArgs e)
        {
            init();
        }
    }
}
