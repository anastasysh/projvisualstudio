﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace snakeExam
{
    public partial class mainMenu : Form
    {
        public mainMenu()
        {
            InitializeComponent();
        }

        private void newGameFunc(object sender, EventArgs e)
        {
            
            Game newGame = new Game();
            newGame.ShowDialog();
        }

        private void SelectLvlFunc(object sender, EventArgs e)
        {
            Select selectLvl = new Select();
            selectLvl.ShowDialog();
        }

        private void tableOfRecordsFunc(object sender, EventArgs e)
        {
            Table tableOfRecords = new Table();
            tableOfRecords.ShowDialog();
        }

        private void customLvlFunc(object sender, EventArgs e)
        {
            menu customLvl = new menu();
            customLvl.ShowDialog();
        }
    }
}
