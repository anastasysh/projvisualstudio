﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labControlMoveOnTimer
{
    public partial class fm : Form
    {
        private int dX = 1;

        public fm()
        {
            InitializeComponent();

            Timer timer = new Timer();
            timer.Interval = 100;
            timer.Tick += Timer_Tick;
            timer.Start();
            this.MouseWheel += (s, e) => dX += (e.Delta > 0) ? 1 : -1;
            this.MouseClick += (s, e) => timer.Enabled = !timer.Enabled;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            var x = pictureBox1;
            if ((x.Location.X + dX + x.Width > this.ClientSize.Width) || (x.Location.X + dX < 0))
            {
                dX *= -1;
            }
            x.Location = new Point(x.Location.X + dX, x.Location.Y);
            Text = $"{Application.ProductName} : {x.Location.X}, {dX}, {x.Width},{this.ClientSize.Width}";
        }
    }
}
