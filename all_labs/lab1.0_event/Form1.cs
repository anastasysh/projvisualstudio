﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1._0_event
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button2.Click += two;// способ 2. присвоение
            button3.Click += delegate //  способ 3. тип анонимная функция.
            {
                MessageBox.Show("способ3");
            };
        }

        private void two(object sender, EventArgs e)
        {
            MessageBox.Show("способ2"); //реализация
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("способ1"); //метод 1. через двойной клик
        }

    }
}
