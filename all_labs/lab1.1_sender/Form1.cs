﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1._1_sender
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button1.Click += Button1_click;
            button2.Click += Button1_click;
            label1.Click += Button1_click;
            label2.Click += Button1_click;
            button3.Click += Button3_click;
        }

        private void Button1_click(object sender, EventArgs e)
        {
            if (sender is Button)
            MessageBox.Show(((Button)sender).Text);
            if (sender is Control)
            MessageBox.Show(((Control)sender).Text);
        }

        private void Button3_click(object sender, EventArgs e)
        {
            
            if (sender is Control)
                MessageBox.Show((sender as Control).Text); // в асемблере добавляются 2-3 строчки надо будет посмотреть
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
