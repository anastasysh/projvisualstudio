﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labStreamWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\tempStream.txt";

            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                streamWriter.WriteLine("¯\\_(ツ)_/¯");
                streamWriter.WriteLine("¯\\_(ツ)_/¯");
                streamWriter.WriteLine("¯\\_(ツ)_/¯");
                Console.WriteLine("Файл создан");
            }
            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine("Ъ!");
                Console.WriteLine("Файл дописан");
            }
            using (StreamReader streamReader = new StreamReader(path))
            {
                Console.WriteLine("--- Begin ---");
                Console.WriteLine(streamReader.ReadToEnd());
                Console.WriteLine("--- End ---");
                Console.WriteLine("Файл считан");
            }
            using (StreamReader streamReader = new StreamReader(path))
            {
                string line;
                int n = 1;
                while ((line = streamReader.ReadLine()) != null)
                {
                    Console.WriteLine($"Строка {n++}: {line}");
                }
                Console.WriteLine("Файл считан построчно");
            }
            var a = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                foreach (var item in a)
                {
                    streamWriter.Write(item);
                }
            }
            Console.ReadLine();
        }
    }
}
