﻿namespace labRoadEditor
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pmenu = new System.Windows.Forms.Panel();
            this.pboard = new System.Windows.Forms.Panel();
            this.bsave = new System.Windows.Forms.Button();
            this.bload = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pmenu
            // 
            this.pmenu.Location = new System.Drawing.Point(12, 12);
            this.pmenu.Name = "pmenu";
            this.pmenu.Size = new System.Drawing.Size(200, 612);
            this.pmenu.TabIndex = 0;
            // 
            // pboard
            // 
            this.pboard.Location = new System.Drawing.Point(218, 12);
            this.pboard.Name = "pboard";
            this.pboard.Size = new System.Drawing.Size(785, 753);
            this.pboard.TabIndex = 1;
            // 
            // bsave
            // 
            this.bsave.Location = new System.Drawing.Point(12, 630);
            this.bsave.Name = "bsave";
            this.bsave.Size = new System.Drawing.Size(200, 36);
            this.bsave.TabIndex = 2;
            this.bsave.Text = "Save";
            this.bsave.UseVisualStyleBackColor = true;
            // 
            // bload
            // 
            this.bload.Location = new System.Drawing.Point(12, 672);
            this.bload.Name = "bload";
            this.bload.Size = new System.Drawing.Size(200, 36);
            this.bload.TabIndex = 3;
            this.bload.Text = "Load";
            this.bload.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 747);
            this.Controls.Add(this.bload);
            this.Controls.Add(this.bsave);
            this.Controls.Add(this.pmenu);
            this.Controls.Add(this.pboard);
            this.Name = "fm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pmenu;
        private System.Windows.Forms.Panel pboard;
        private System.Windows.Forms.Button bsave;
        private System.Windows.Forms.Button bload;
    }
}

