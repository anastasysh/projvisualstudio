﻿using labRoadEditor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRoadEditor
{
    public partial class fm : Form
    {
        private Bitmap b;
        private Bitmap m;
        public bool isPressed = false;
        Point CurrentPoint;
        Point PrevPoint;
        Graphics g; 
        private Dictionary<char, Rectangle> pics = new Dictionary<char, Rectangle>();
        private Dictionary<int, char> bind = new Dictionary<int, char>();
        char[] ims = { 'h', '1', 'c', 'v', '4', 'n', '2', '3', 'r', 'u', 'd', 'l' };
        string path = @"C:\Users\User\source\repos\projvisualstudio\labRoadEditor\dict.txt";
        Image img = Resources.roads;
        public int Rows { get; private set; } = 10;
        public int Cols { get; private set; } = 10;
        public int xCW;
        public int xCH;
        public int selected;
        private Point StartPoint;
        public Dictionary<int[], char> dict = new Dictionary<int[], char>();


        public fm()
        {
            InitializeComponent();
            DictFill();
            DrawCells();
            DrawMenu();
            bsave.Click += Bsave_Click;
            bload.Click += Bload_Click;
            pboard.Paint += Pboard_Paint;
            pmenu.Paint += Pmenu_Paint;
            pmenu.MouseUp += Pmenu_MouseUp;
            pboard.MouseUp += Pboard_MouseUp;
            pboard.MouseMove += Pboard_MouseMove;
            pboard.MouseDown += Pboard_MouseDown;
        }

        private void Pboard_MouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
            isPressed = true;
        }

        private void Pboard_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                if (sender is Control)
                {
                    ((Control)sender).Location = PointToClient(x);
                }
            }
            if (e.Button == MouseButtons.Left)
            {
                if (isPressed) {
                    Pboard_MouseUp(sender, e);
                };
            }
        }

        private void Bload_Click(object sender, EventArgs e)
        {
            dict.Clear();
            using (var reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var a = line.Split('\t');
                    int[] arr = { int.Parse(a[0]), int.Parse(a[1]) };
                    dict.Add(arr, char.Parse(a[2]));
                }
            }
            DrawCells();
        }

        private void Bsave_Click(object sender, EventArgs e)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (var kvp in dict)
                {
                    writer.WriteLine($"{kvp.Key[0]}\t{kvp.Key[1]}\t{kvp.Value}");
                }
            }
        }

        private void Pboard_MouseUp(object sender, MouseEventArgs e)
        {
            if (selected != 0 && e.Button == MouseButtons.Left)
            {
                int x, y;
                x = e.X / (pboard.Width / Cols);
                y = e.Y / (pboard.Height / Rows);
                
                using (var g = Graphics.FromImage(b))
                {
                    g.Clear(DefaultBackColor);
                    g.DrawImage(img,
                            new Rectangle(xCW * x, xCH * y, xCW, xCH),
                            pics[bind[selected]],
                            GraphicsUnit.Pixel);
                    this.Refresh();
                    PaintEventArgs s = new PaintEventArgs(g, new Rectangle(0, 0, pboard.Width, pboard.Height));
                    Pboard_Paint(pboard, s);
                }
                int[] arr = { x, y };
                dict.Add(arr, bind[selected]);
                DrawCells();
            }
        }

        private void Pmenu_MouseUp(object sender, MouseEventArgs e)
        {
            int x, y;
            if (e.X < pmenu.Width / 2)
                x = 1;
            else
                x = 2;
            y = e.Y / (pmenu.Height / 6);
            selected = x * 10 + y;
            System.Console.WriteLine($"{selected}");

        }

        private void Pmenu_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(m, 0, 0);
        }

        private void Pboard_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, 0, 0);
        }

        private void DrawMenu()
        {
            m = new Bitmap(pmenu.Width, pmenu.Height);
            using (var g = Graphics.FromImage(m))
            {
                g.Clear(DefaultBackColor);
                int k = 0;
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        g.DrawImage(img,
                        new Rectangle(pmenu.Width / 2 * i, pmenu.Height / 6 * j, pmenu.Width / 2, pmenu.Width / 2),
                        pics[ims[k]],
                        GraphicsUnit.Pixel);
                        System.Console.WriteLine($"{pmenu.Width / 2 * j}, {pmenu.Height / 6 * i}");
                        k++;
                    }
                }
            }
        }

        private void DrawCells()
        {
            b = new Bitmap(pboard.Height, pboard.Width);
            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i <= Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), 0, i * xCH, Cols * xCW, i * xCH);
                }
                for (int i = 0; i <= Cols; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), i * xCW, 0, i * xCW, Rows * xCH);
                }
                g.DrawLine(new Pen(Color.Green, 1), 0, Cols * xCH - 1, Cols * xCW, Cols * xCH - 1);
                g.DrawLine(new Pen(Color.Green, 1), Rows * xCW - 1, 0, 10 * xCW - 1, Rows * xCH);
                
                int q, w;
                q = 2;
                w = 2;
                g.DrawImage(img,
                       new Rectangle(xCW * q, xCH * w, xCW, xCH),
                       pics[bind[11]],
                       GraphicsUnit.Pixel);
                      
                foreach (KeyValuePair<int[],char> kvp in dict)
                {
                    g.DrawImage(img,
                            new Rectangle(xCW * kvp.Key[0], xCH * kvp.Key[1], xCW, xCH),
                            pics[kvp.Value],
                            GraphicsUnit.Pixel);
                }

                pboard.Refresh();

            }
        }

        private void DictFill()
        {
            pboard.Height = 550;
            pboard.Width = 550;
            xCW = pboard.Width / Cols;
            xCH = pboard.Height / Rows;
            img = ResizeImage(img, pboard.Width, pboard.Height);
            int k = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Rectangle tmp;
                    tmp = new Rectangle(j * pboard.Width / 4, i * pboard.Height / 3, pboard.Width / 4, pboard.Height / 3);
                    pics.Add(ims[k], tmp);
                    if (k + 10 < 16)
                    {
                        bind.Add(k + 10, ims[k]);
                    }
                    else
                    {
                        bind.Add(k - 6 + 20, ims[k]);
                    }
                    
                    k++;
                }
            }
        }

        private Image ResizeImage(Image img, int newWidth, int newHeight)
        {
            Image thumbImg = new Bitmap(newWidth, newHeight);

            using (Graphics gr = Graphics.FromImage(thumbImg))
            {
                gr.DrawImage(img, new Rectangle(0, 0, newWidth, newHeight), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            }
            return thumbImg;
        }
    }
}
