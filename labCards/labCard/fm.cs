﻿using labCard.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCard
{
    public partial class fm : Form
    {
        private Cards cards;
        public int rotation;
        public int [] map = new int[6] {0,0,0,0,0,0};
        public fm()
        {
            InitializeComponent();
            pictureBox1.BackgroundImage = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.BackgroundImageLayout = ImageLayout.None;
            randCard();
            cards = new Cards(Resources.cards, 4, 13);
            pictureBox1.Paint += PictureBox1_Paint;
            button1.Click += button1_click;
                      
        }

        private void button1_click(object sender, EventArgs e)
        {
            randCard();
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(cards[0].Width, cards[0].Height);
            e.Graphics.RotateTransform(rotation);
            Random rnd = new Random();
            for (int i = 0; i < 6; i++)
            {
                e.Graphics.RotateTransform(rotation);
                e.Graphics.DrawImage(cards[map[i]],-cards[0].Width + 80, -cards[0].Height + 60);
            }
            
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            rotation = trackBar1.Value;
            pictureBox1.Refresh();
        }
        private void randCard()
        {
            Random rnd = new Random();
            for (int i = 0; i < 6; i++)
            {
                map[i] = rnd.Next(35);
            }
            pictureBox1.Refresh();
        }
    }
    
}
