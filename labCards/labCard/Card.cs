﻿using System;
using System.Drawing;

namespace labCard
{
    internal class Cards
    {
        private Bitmap[] cards;

        public int Rows { get; }
        public int Cols { get; }
        public Cards(Bitmap img, int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
            NewCords(img);
        }
        public Bitmap this[int index]
        {
            get
            {
                return cards[index];
            }
        }
        private void NewCords(Image image)
        {
            var b = new Bitmap(image);
            int w = b.Width / Cols;
            int h = b.Height / Rows;
            int n = 0;
            cards = new Bitmap[Rows * Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    cards[n] = new Bitmap(w, h);
                    var g = Graphics.FromImage(cards[n]);
                    g.DrawImage(b, 0, 0, new Rectangle(j * w, i * h, w, h), GraphicsUnit.Pixel);
                    g.Dispose();
                    n++;
                }
            }
        }
    }
}