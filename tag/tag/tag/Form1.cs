﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;


namespace tag
{
    public partial class Form1 : Form
    {
        GameLogic game = new GameLogic();
        public Form1()
        {
            InitializeComponent();
            label1.Click += swap;
            label2.Click += swap;
            label3.Click += swap;
            label4.Click += swap;
            label5.Click += swap;
            label6.Click += swap;
            label7.Click += swap;
            label8.Click += swap;
            label9.Click += swap;
            label10.Click += swap;
            label11.Click += swap;
            label12.Click += swap;
            label13.Click += swap;
            label14.Click += swap;
            label15.Click += swap;
            label16.Click += swap;
            newGame.Click += restart;
            restart();
        }

        private void swap(object sender, EventArgs e) 
        {            
            if (sender is Label) {
                Label lb = sender as Label;
                System.Drawing.Point XY = lb.Location;
                int pos = game.get_pos(lb.Name);
                List <int> maybeFree = game.check_maybe_free(pos);
                for (int i = 0; maybeFree.Count != i; i++)
                {
                    Label ll = label(maybeFree[i]);
                    if (ll.Text == "") {
                        string old = lb.Text;
                        ll.Text = old;
                        lb.ResetText();
                        lb.Refresh();
                        string name = ll.Name.Replace("label", "");
                        if (ll.Text == name){
                        ll.BackColor = Color.LightGreen;
                        } else {
                        lb.BackColor = Color.Empty;
                        }
                        break;
                    }  
                }
            }
            List<string> allName = new List<string>();
            List<string> allText = new List<string>();
            for (int i = 0; i < 15; i++)
            {
                Label ll = label(i + 1);
                allName.Add(ll.Name);
                allText.Add(ll.Text);
            }
            
            if (game.check_result(allName, allText) == 1) {
                MessageBox.Show(
            "Вы выиграли",
            "",
            MessageBoxButtons.OK,
            MessageBoxIcon.Warning,
            MessageBoxDefaultButton.Button1,
            MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        Label label(int pos) {
            switch (pos)
            {
                case 1: return label1;
                case 2: return label2;
                case 3: return label3;
                case 4: return label4;
                case 5: return label5;
                case 6: return label6;
                case 7: return label7;
                case 8: return label8;
                case 9: return label9;
                case 10: return label10;
                case 11: return label11;
                case 12: return label12;
                case 13: return label13;
                case 14: return label14;
                case 15: return label15;
                case 16: return label16;
                default: return null;
            }
        }
        private void restart(object sender, EventArgs e)
        {
        List<int> randList = game.restart();
            for (int i=0; i<randList.Count; i++){
            label(i+1).Text = randList[i].ToString();

            string name = label(i+1).Name.Replace("label", "");
            if (label(i+1).Text == name){
            label(i+1).BackColor = Color.LightGreen;
            } else {
            label(i+1).BackColor = Color.Empty;
            }
            label(i+1).Refresh();
            }
            label16.BackColor = Color.Empty;
            label16.Refresh();
        }
        private void restart()
        {
        List<int> randList = game.restart();
            for (int i=0; i<randList.Count; i++){
            label(i+1).Text = randList[i].ToString();

            string name = label(i+1).Name.Replace("label", "");
            if (label(i+1).Text == name){
            label(i+1).BackColor = Color.LightGreen;
            } else {
            label(i+1).BackColor = Color.Empty;
            }
            label(i+1).Refresh();
            }
            label16.BackColor = Color.Empty;
            label16.Refresh();
        }
    }
}
