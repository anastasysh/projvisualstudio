﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;


namespace tag
{
    class GameLogic
    {       
        
        int size = 4;
        static Random rnd = new Random();
        List<int> mapList = new List<int>(){1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        
        public List <int> check_maybe_free(int pos) {
            List <int> maybeFreePos=new List<int>();
            if (pos > size) {

               maybeFreePos.Add((pos - size));
            }
            if (pos <= ((size - 1) * size)) {
                maybeFreePos.Add((pos + 4));
            }
            if (pos % size != 0) {
                maybeFreePos.Add((pos + 1));
            }
            if (pos % size != 1)
            {
                maybeFreePos.Add((pos - 1));
            }
            return maybeFreePos;
        }
        public List <int> restart()
        {
            List<int> randList = new List<int>();
            List<int> temp = mapList;
            randList = temp.OrderBy(x => rnd.Next()).ToList<int>();
            return randList;
        }
        int check_duplicate(int num, List<int> randList) 
        { 
        if (num == 0)
            {
                return 1;
            }
            for (int i = 0; i < randList.Count; i++) {
                if (num == randList[i]) {
                    return 1;
                }
            }
            return 0; 
        }

        public int get_pos(string name) {
            int pos = int.Parse(name.Replace("label", ""));
            return pos;
        }
        System.Drawing.Point get_point(int pos) {
            int y = 0;
            int x = 0;
            System.Drawing.Point ret = new System.Drawing.Point (0, 0);
            if (pos % size == 0) {
                x = 163 * (pos / size) + 3;
                y = (4-1)*120;
            }
            else {
                x = 163*((pos / size) + 1)+3;
                y = ((pos % size) - 1) * 120; 
            }
            ret.X = x;
            ret.Y = y;
            return ret;
        }
        public int check_result(List<string> allName, List<string> allText) {
            int win = 0;

            for (int i = 0; i < allName.Count; i++)
            {
                string thisName = allName[i];
                thisName = thisName.Replace("label", "");
                string thisText = allText[i];
                if (thisText != "")
                {
                    if (int.Parse(thisName) == int.Parse(thisText))
                    {
                        win += 1;
                    }
                }
                else {
                    if (int.Parse(thisName) == 16)
                    {
                        win += 1;
                    }

                }
            }
            if (win == 15)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

}

